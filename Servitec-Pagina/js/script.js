function enlaces() {
  /*NAV*/
  $('a#menuNavI').attr("href","index.html");
  $('a#menuNavN').attr("href","nosotros.html");
  $('a#menuNavT').attr("href","trabajos.html");
  $('a#menuNavC').attr("href","consejosLadrillos.html");
  $('a#menuNavCr').attr("href","creadores.html");
  $('a#menuNavCn').attr("href","contactos.html");

  /*IMAGEN DEL NAV*/
  $('a#imagenLink').attr("href","index.html");

  /*FOOTER DE INDEX*/
  $('a#footerI').attr("href","index.html");
  $('a#footerN').attr("href","nosotros.html");
  $('a#footerT').attr("href","trabajos.html");
  $('a#footerC').attr("href","consejosLadrillos.html");
  $('a#footerCn').attr("href","contactos.html");

  /*LEER MAS DE INDEX*/
  $('a#masCr').attr("href","creadores.html");
  $('a#masN').attr("href","nosotros.html");
  $('a#masCn').attr("href","contactos.html");

  /*ASIDE CONSEJOS*/
  $('a#consejosL').attr("href","consejosLadrillos.html");
  $('a#consejosU').attr("href","consejosConstruccionCasa.html");
  $('a#consejosD').attr("href","consejosDiseno.html");
  $('a#consejosP').attr("href","consejosPintura.html");

  /*BUSQUEDA*/
  $('a#busI').attr("href","index.html");
  $('a#busN').attr("href","nosotros.html");
  $('a#busT').attr("href","trabajos.html");
  $('a#busL').attr("href","consejosLadrillos.html");
  $('a#busCon').attr("href","consejosLadrillos.html");
  $('a#busCr').attr("href","creadores.html");
  $('a#busC').attr("href","contactos.html");
  $('a#busUC').attr("href","consejosUbicacionCasa.html");
  $('a#busD').attr("href","consejosDiseno.html");
  $('a#busP').attr("href","consejosPintura.html");
};

function visitas() {
  if(localStorage.contadorCuadro){
    localStorage.contadorCuadro = 1 + parseInt(localStorage.contadorCuadro);
  } else {
    localStorage.contadorCuadro = 1;
  }

  document.getElementById('contadorCuadro').innerHTML = localStorage.contadorCuadro;
}

function busqueda() {
  var buscador = $("#table").DataTable();
  $("#busqueda").keyup(function(){
    buscador.search($(this).val()).draw();

    if ($("#busqueda").val() == ""){
      $(".content-search").fadeOut(300);
    } else {
      $(".content-search").fadeIn(300);
    }

  })
}

function timeline(fecha, titulo, mensaje) {
   var li = $("<li/>", {
    });

    var time = $("<time/>", {
      "class": "cbp_tmtime"
     });

    var span1 = $("<span/>", {
       html: fecha
     });

    var span2 = $("<span/>", {
      });

    var div1 = $( "<div/>", {
        "class": "cbp_tmicon cbp_tmicon-screen"
      });

    var div2 = $( "<div/>", {
        "class": "cbp_tmlabel"
      });

    var h = $("<h2/>",{
      html: titulo
    });

    var p = $("<p/>",{
      html: mensaje
    })

    span1.appendTo(time);
    span2.appendTo(time);
    h.appendTo(div2);
    p.appendTo(div2);
    time.appendTo(li);
    div1.appendTo(li);
    div2.appendTo(li);
    li.appendTo("#timeline .cbp_tmtimeline");
}

function cargarTimeline() {
    $.getJSON( "js/timeline.json", function( data ) {
      $.each( data, function( key, val) {
          timeline(val["fecha"], val["titulo"], val["mensaje"])
      });
  });
}

$(document).ready(function(){
  enlaces();
  cargarTimeline();
  visitas();
  busqueda();
  //slider
  classToggle();
  document.querySelector('#toggle-animation').addEventListener('click', classToggle);
});


//slider
function classToggle() {
var el = document.querySelector('.icon-cards__content');
el.classList.toggle('step-animation');
}
